# README #


### What is this repository for? ###

* ThruTracker
* Version 1.6
* For more information: www.sonarjamming.com/thrutracker

### How do I get set up? ###

* This code requires Matlab version 2019b or newer

### Contribution guidelines ###

* Code was written by Dr. Aaron Corcoran, Dr. Tyson Hedrick and others

### Who do I talk to? ###

* Contact Aaron Corcoran (acorcora@uccs.edu) for more information

# This code will be published with an open source license. You are free to use this code for your own personal uses as long as it is not monetized in any way.